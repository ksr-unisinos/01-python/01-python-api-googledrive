from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


from folder import folder as f
import csv

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/activity']


n_registros = 5000
timeL=[]
userL=[]
eventL=[]
targetL=[]
mimeTypeL=[]

def removeBadChar(temp = 'null'):
	temp = temp.replace('\u0303','')
	temp = temp.replace('\u2206','')
	temp = temp.replace('\u0302','')
	temp = temp.replace('\u0301','')
	temp = temp.replace('\u0327','')
	temp = temp.replace('\u0308','')
	temp = temp.replace('\u200e','')
	
	return temp

def main():
	"""Shows basic usage of the Drive Activity API.
	Prints information about the last 10 events that occured the user's Drive.
	"""
	creds = None
	# The file token.pickle stores the user's access and refresh tokens, and is
	# created automatically when the authorization flow completes for the first
	# time.
	if os.path.exists('token.pickle'):
		with open('token.pickle', 'rb') as token:
			creds = pickle.load(token)
	# If there are no (valid) credentials available, let the user log in.
	if not creds or not creds.valid:
		if creds and creds.expired and creds.refresh_token:
			creds.refresh(Request())
		else:
			flow = InstalledAppFlow.from_client_secrets_file(
				'credentials.json', SCOPES)
			creds = flow.run_local_server(port=0)
		# Save the credentials for the next run
		with open('token.pickle', 'wb') as token:
			pickle.dump(creds, token)

	service = build('appsactivity', 'v1', credentials=creds)

	for x in range(len(f)):
		print(f[x][0])
		# Call the Drive Activity API
		results = service.activities().list(
			source='drive.google.com',
			groupingStrategy= 'none',
			drive_ancestorId=f[x][1],
			pageSize=n_registros
			).execute()
		activities = results.get('activities', [])

		if not activities:
			print('No activity.')
		else:
			#print('Recent activity:')
			for activity in activities:
				event = activity['combinedEvent']
				user = event.get('user', None)
				target = event.get('target', None)
				if user is None or target is None:
					continue
				time = datetime.datetime.fromtimestamp(
					int(event['eventTimeMillis'])/1000)
				timeL.append(time)
				userL.append(removeBadChar(user['name']))
				eventL.append(removeBadChar(event['primaryEventType']))
				targetL.append(removeBadChar(target['name']))
				mimeTypeL.append(removeBadChar(target['mimeType']))
				print(u'{0}: {1}, {2}, {3} ({4})'.format(time, user['name'],
					event['primaryEventType'], target['name'], target['mimeType']))

	with open('output.csv', 'w') as csvfile:
		filewriter = csv.writer(csvfile, delimiter=';',lineterminator = '\n',quotechar='|', quoting=csv.QUOTE_MINIMAL)
		filewriter.writerow(['time', 'name','primaryEventType','name','mimeType'])
		for linha in range (len(userL)):
			filewriter.writerow([
				str(timeL[linha])[:19],
				userL[linha],
				eventL[linha],
				targetL[linha],
				mimeTypeL[linha]
				])

if __name__ == '__main__':
	main()