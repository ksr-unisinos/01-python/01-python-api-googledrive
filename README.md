# 01-python-Api-googledrive

Exste exemplo explora o relatório de atividades do google Drive

Para instalar esta API execute:

```pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib```



Todas as informações de instalação estão também no link abaixo:

https://developers.google.com/drive/activity/v1/quickstart/python



Para outras opções da API, como manipular arquivos, verifique a documentação abaixo

https://developers.google.com/drive/api/v3/quickstart/python

https://www.thepythoncode.com/article/using-google-drive--api-in-python


